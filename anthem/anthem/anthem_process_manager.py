from flask import g
from dateutil.parser import parse


class AnthemDashboardManager(object):

    def get_base_query(self):
        return {
            "query": {
                "bool": {
                    "must": [
                    ]
                }
            },
            "aggs": {
                "distinct_colors": {
                    "terms": {
                        "field": "processName",
                        "size": 1000
                    }
                }
            }
        }

    def apply_filters_to_base_query(self, args):
        time_filters = {
            "range": {
                "@timestamp": {
                    "gte": "now-1y"
                }
            }
        }
        claim_type_filter = {
            "multi_match": {
                "query": "",
                "fields": ["CLAIM_TYPE"]
            }
        }
        if 'from' in args:
            time_filters['range']['@timestamp']['lte'] = args['from']

        if 'to' in args:
            time_filters['range']['@timestamp']['gte'] = args['to']

        if 'claim_type' in args:
            claim_type_filter['multi_match']['query'] = args['claim_type']
            self.get_base_query()['query']['bool']['must'].append(claim_type_filter)

        self.get_base_query()['query']['bool']['must'].append(time_filters)

        return self.get_base_query()

    def get_total_count_by_process(self, args):
        query = self.apply_filters_to_base_query(args)
        result = g.es_session.search(index="bluck_test_data", body=query)
        return result['aggregations']['distinct_colors']['buckets']

    def get_success_count_by_process(self, args):
        success_filter = {
            "multi_match": {
                "query": "210",
                "fields": ["code"]
            }
        }
        query = self.apply_filters_to_base_query(args)
        query['query']['bool']['must'].append(success_filter)
        result = g.es_session.search(index="bluck_test_data", body=query)
        return result['aggregations']['distinct_colors']['buckets']

    def get_failed_count_by_process(self, args):
        failed_filter = {
            "multi_match": {
                "query": "400",
                "fields": ["code"]
            }
        }
        query = self.apply_filters_to_base_query(args)
        query['query']['bool']['must'].append(failed_filter)
        result = g.es_session.search(index="bluck_test_data", body=query)
        return result['aggregations']['distinct_colors']['buckets']

    def _append_pie_chart_data(self, payload):
        pass

    def _append_bar_chart_data(self, payload):
        pass

    def fetch_claims(self, args):
        total_claim_counts = self.get_total_count_by_process(args)
        success_claim_counts = self.get_success_count_by_process(args)
        failed_claims_count = self.get_failed_count_by_process(args)

        payload = {'subway_map': {}}

        for process in total_claim_counts:
            in_count = process['doc_count']
            out_count = next((i['doc_count'] for i in success_claim_counts if i['key'] == process['key']), 0)
            failed_count = next((i['doc_count'] for i in failed_claims_count if i['key'] == process['key']), 0)
            active_count = in_count - (out_count + failed_count)

            process_payload = {
                process['key'].upper(): {
                    'in': in_count,
                    'out': out_count,
                    'failed_count': failed_count,
                    'active_count': active_count,
                    'status': 'ACTIVE_FLOWING',
                    'sub_process': []
                }
            }
            payload['subway_map'].update(process_payload)

        return self.prepare_common_json(payload, args)

    def prepare_common_json(self, payload, args):
        common_json = {
            'subway_map': {
                'EDI': {
                    'in': payload['subway_map']['EDI_837_INGESTION']['in'],
                    'out': payload['subway_map']['EDI_837_INGESTION']['out'],
                    'failed_count': payload['subway_map']['EDI_837_INGESTION']['failed_count'],
                    'active_count': payload['subway_map']['EDI_837_INGESTION']['active_count'],
                    'status': 'ACTIVE_FLOWING',
                    'display_name': 'EDI',
                    'sub_process': [
                        {'EDI_837_INGESTION': payload['subway_map']['EDI_837_INGESTION']}
                    ]
                },
                'CIW': {
                    'in': payload['subway_map']['CLAIM_CREATION']['in'],
                    'out': payload['subway_map']['FILE_GENERATION']['out'],
                    'failed_count': payload['subway_map']['FILE_GENERATION']['failed_count'],
                    'active_count': payload['subway_map']['FILE_GENERATION']['active_count'],
                    'status': 'ACTIVE_FLOWING',
                    'display_name': 'CIW',
                    'sub_process': [
                        {'CLAIM_CREATION': payload['subway_map']['CLAIM_CREATION']},
                        {'ORCHESTRATION': payload['subway_map']['ORCHESTRATION']},
                        {'OBJECT_GENERATION': payload['subway_map']['OBJECT_GENERATION']},
                        {'FILE_GENERATION': payload['subway_map']['FILE_GENERATION']},
                    ]
                },
                'WGS': {
                    'in': payload['subway_map']['WGS_PREMAPPER_POSTBACK']['in'],
                    'out': payload['subway_map']['WGS_EDIT_ADJUDICATION_FINALIZATION']['out'],
                    'failed_count': payload['subway_map']['WGS_EDIT_ADJUDICATION_FINALIZATION']['failed_count'],
                    'active_count': payload['subway_map']['WGS_EDIT_ADJUDICATION_FINALIZATION']['active_count'],
                    'status': 'ACTIVE_FLOWING',
                    'display_name': 'WGS',
                    'sub_process': [
                        {'POST_BACK_1': payload['subway_map']['WGS_PREMAPPER_POSTBACK']},
                        {'POST_BACK_2': payload['subway_map']['WGS_LOAD_POSTBACK']},
                        {'WGS': payload['subway_map']['WGS_EDIT_ADJUDICATION_FINALIZATION']}
                    ]
                }
            },
            'pie_chart': [
                {
                    "name": "EDI",
                    "label": "WorkItems Received",
                    "total_count": payload['subway_map']['EDI_837_INGESTION']['failed_count'],
                    "color": "#22cdd9",
                    "kpi": "10%"
                },
                {
                    "name": "CIW",
                    "label": "WorkItems Received",
                    "total_count": payload['subway_map']['FILE_GENERATION']['failed_count'],
                    "color": "#e4a130",
                    "kpi": "10%"
                },
                {
                    "name": "WGS",
                    "label": "WorkItems Loaded",
                    "total_count": payload['subway_map']['WGS_EDIT_ADJUDICATION_FINALIZATION']['failed_count'],
                    "color": "#a81882",
                    "kpi": "10%"
                }
            ],
            "barchart": self.bar_chart_data(args)
        }
        return common_json

    def bar_chart_data(self, args):
        query = {
            "query": {
                "bool": {
                    "must": [
                        {
                            "range": {
                                "@timestamp": {
                                    "gte": "now-1y"
                                }
                            }
                        },
                        {
                            "multi_match": {
                                "query": "210",
                                "fields": ["code"]
                            }

                        }
                    ]
                }
            },
            "aggs": {
                "byday": {
                    "date_histogram": {
                        "field": "@timestamp",
                        "interval": "month"
                    }
                }
            }
        }

        if all([args['from'], args['to']]):
            age_diff = (parse(args['to']) - parse(args['from'])).days
        else:
            age_diff = 0

        if age_diff > 1 and age_diff <= 7:
            query['query']['bool']['must'][0]['range']['@timestamp']['gte'] = 'now-1m'
            query['aggs']['byday']['date_histogram']['interval'] = 'day'
        elif age_diff >=7 and age_diff <= 31:
            query['query']['bool']['must'][0]['range']['@timestamp']['gte'] = 'now-3m'
            query['aggs']['byday']['date_histogram']['interval'] = 'week'
        elif age_diff > 31:
            query['query']['bool']['must'][0]['range']['@timestamp']['gte'] = "now-1y"
            query['aggs']['byday']['date_histogram']['interval'] = 'year'
        else:
            query['query']['bool']['must'][0]['range']['@timestamp']['gte'] = 'now-1d'
            query['aggs']['byday']['date_histogram']['interval'] = 'hour'

        result = g.es_session.search(index="bluck_test_data", body=query)

        return result['aggregations']['byday']['buckets']
