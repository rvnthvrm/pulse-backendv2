from flask import Flask
from flask import g
from elasticsearch import Elasticsearch
from . anthem_process_manager import AnthemDashboardManager
import pymongo


app = Flask(__name__)


@app.before_request
def request(exception=None):
    g.es_session = Elasticsearch(['10.32.102.180'], port=9200)
    g.mongo_session = pymongo.MongoClient("localhost", 27017)


@app.teardown_request
def response(exception=None):
    g.es_session.transport.close()
    g.mongo_session.close()


data = {
    "subway_map": {
        "EDI_837_INGESTION": {
            "in": 10,
            "out": 7,
            "failed_count": 2,
            "drop_counts": 1,
            "display_name": "EDI",
            "active_count": 10,
            "processes": 1,
            "status": "ACTIVE_FLOWING",  # TODO: need to change
            "process_list": [
                {
                    "EDI_837_FILE_INJESTION": {
                        "in": 10,
                        "out": 7,
                        "failed_count": 2,
                        "drop_counts": 1,
                        "active_count": 10,
                    }
                }
            ]
        },
        "CLAIM_CREATION": {
            "in": 10,
            "out": 7,
            "failed_count": 2,
            "drop_counts": 1,
            "display_name": "CIW",
            "active_count": 10,
            "processes": 4,
            "status": "ACTIVE_FLOWING",  # TODO: need to change
            "process_list": [
                {
                    "CLAIM_CREATION": {
                        "in": 10,
                        "out": 7,
                        "failed_count": 2,
                        "drop_counts": 1,
                        "active_count": 10,
                    }
                },
                {
                    "ORCHESTRATION": {
                        "in": 10,
                        "out": 7,
                        "failed_count": 2,
                        "drop_counts": 1,
                        "active_count": 10,
                    }
                },
                {
                    "OBJECT_GENERATION": {
                        "in": 10,
                        "out": 7,
                        "failed_count": 2,
                        "drop_counts": 1,
                        "active_count": 10,
                    }
                },
                {
                    "FILE_GENERATION": {
                        "in": 10,
                        "out": 7,
                        "failed_count": 2,
                        "drop_counts": 1,
                        "active_count": 10,
                    }
                }
            ]
        },
        "ORCHESTRATION": {
            "in": 10,
            "out": 7,
            "failed_count": 2,
            "drop_counts": 1,
            "display_name": "WGS",
            "active_count": 10,
            "processes": 4,
            "Processes": 1,
            "status": "ACTIVE_FLOWING",  # TODO: need to change
            "process_list": [
                {
                    "wgs_premapper_postback": {
                        "in": 10,
                        "out": 7,
                        "failed_count": 2,
                        "drop_counts": 1,
                        "active_count": 10,
                    }
                },
                {
                    "POST_BACK_2": {
                        "in": 10,
                        "out": 7,
                        "failed_count": 2,
                        "drop_counts": 1,
                        "active_count": 10,
                    }
                },
                {
                    "WGS": {
                        "in": 10,
                        "out": 7,
                        "failed_count": 2,
                        "drop_counts": 1,
                        "active_count": 10,
                    }
                }
            ]
        }
    },
    "pie_chart": [
        {
            "name": "EDI",
            "label": "WorkItems Received",
            "total_count": 10,
            "color": "1879bf",
            "kpi": "10%"
        },
        {
            "name": "WGS",
            "label": "WorkItems Loaded",
            "total_count": 10,
            "color": "a81882",
            "kpi": "10%"
        }
    ],
    "bar_chart": [
        {
            "name": "Received at CIW",
            "counts": [
                {
                    "sun": 10,
                    "date": "12/1"
                },
                {
                    "mon": 20,
                    "date": "12/1"
                }
            ]
        }
    ]
}



@app.route('/', methods=['GET'])
def hello():
    from flask import request
    from flask import jsonify, make_response
    am = AnthemDashboardManager()
    count = am.fetch_claims(request.args)
    return count
