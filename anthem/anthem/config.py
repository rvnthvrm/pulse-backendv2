STATUS_CODE_PER_PROCESS = {
    'EDI_837_INGESTION': {'success': '', 'failure': ''},
    'CLAIM_CREATION': {'success': '', 'failure': ''},
    'ORCHESTRATION': {'success': '', 'failure': ''},
    'Object_Generation': {'success': '', 'failure': ''},
    'FILE_GENERATION': {'success': '', 'failure': ''},
    'WGS_POSTBACK_1': {'success': '', 'failure': ''},
    'WGS_LOAD_POSTBACK': {'success': '', 'failure': ''},
    'WGS_EDIT_ADJUDICATION_FINALIZATION': {'success': '', 'failure': ''},

}
