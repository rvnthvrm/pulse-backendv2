from celery import Celery
from elasticsearch import Elasticsearch
from . import celeryconfig
from celery.signals import task_prerun, task_postrun
from flask import g


celery = Celery(
    __name__,
    broker=celeryconfig.BROKER_URL,
)
celery.config_from_object('celerytasks.celeryconfig')


@task_prerun.connect()
def task_prerun(task_id, task, *args, **kwargs):
    g.es_session = Elasticsearch(['10.32.102.180'], port=9200)


@task_postrun.connect()
def task_postrun(task_id, task, retval, state, *args, **kwargs):
    g.es_session.transport.close()


@celery.task
def aggregate_anthems_data():
    pass

