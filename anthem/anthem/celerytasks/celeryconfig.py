# Celery configuration

BROKER_URL = 'mongodb://localhost:27017/anthem'
CELERY_RESULT_BACKEND = "mongodb"
CELERY_MONGODB_BACKEND_SETTINGS = {
    "host": "127.0.0.1",
    "port": 27017,
    "database": "anthem",
    "taskmeta_collection": "results",
}

# json serializer is more secure than the default pickle
CELERY_TASK_SERIALIZER = 'json'
CELERY_ACCEPT_CONTENT = ['json']
CELERY_RESULT_SERIALIZER = 'json'

# Use UTC instead of localtime
CELERY_ENABLE_UTC = True
