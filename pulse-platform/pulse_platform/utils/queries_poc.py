from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
from elasticsearch_dsl.query import MultiMatch
from elasticsearch_dsl import Q


es = Elasticsearch(['10.32.102.180'], port=9200)
p1 = {
    'foo': 'ABC'
}
p2 = {
    'foo': 'AB'
}
p3 = {
    'foo': 'CDA'
}
p4 = {
    'foo': 'CD'
}

body = {
  "data": [
    {
      "workitem_start_status_timestamp": "2019/09/14 06:43:25",
      "workaction_name": "EDI 837 INGESTION",
      "workaction_instance_name": "X12BatchClaimAdjudication2 cf75035d916c60bbd9ea6beff61a963c",
      "workitem_unique_identifier": "CDE55842149",
      "workitem_name": "ITS HOST Institutional",
      "workitem_metadata": {
        "RECORDSTATUS": "0",
        "FILEID": "N837File21416",
        "FILELISTENERNODEID": "46da9a7cb27a3ed61f6c70ec76a9968c",
        "EXCEPTIONMESSAGE": "",
        "CLAIMWORKID": "",
        "@timestamp": "2019/09/14 06:43:25"
      }
    }
  ]
}

def testdata():
    es.index(
        index='test_data_6',
        doc_type='workitems',
        id=1,
        body=body
    )



search = Search(using=es, index='c')


def poc():
    es.index(index='revanth', doc_type='a', id=1, body={'foo': 0, 'name': 'Rev'})


query = {
    "query":
        {
            "bool": {
                "should": []
            }
        }
}


query['query']['bool']['should'].append(
    {
        "query_string": {
            "default_field": "foo",
            "query": "*{}*".format(1)
        }
    }
)


query = {
    "query": {
        "bool": {
            "must": [
                {
                    "range": {
                        "@timeStamp": {
                            "gte": "2019-10-02",
                            "lte": "2019-12-04"
                        }
                    }
                },
                {
                    "multi_match": {
                        "query": "EDI_837_INGESTION",
                        "fields": ["processName"]
                    }

                },
                {
                    "multi_match": {
                        "query": "Commercial Institutional",
                        "fields": ["CLAIM_TYPE"]
                    }
                }
            ]
        }
    },
    "size": 10,
    "from": 0,
    "sort": [
        {
            "@timestamp": {
                "order": "asc"
            }
        }
    ]
}

query2 = (
    Q(
        'bool',
        must=[
            Q('multi_match', query='EDI_837_INGESTION', fields=['processName']),
            Q('multi_match', query='EDI_837_INGESTION', fields=['processName']),
            Q('range', **{'@timestamp': {'lt': 'now'}})
        ]
    )
)

query2 = Q(
    'bool',
    must=[Q('multi_match', query=1, fields=['foo']) | Q('multi_match', query=3, fields=['foo']) | Q('range', **{'foo': {'lte': 9, 'gte': 3}})],
)




def create_index():
    es.index(index='b', doc_type='a', id=1, body={'foo': 0, 'name': 'Rev'})
    es.index(index='b', doc_type='a', id=2, body={'foo': 0, 'name': 'PUR'})
    es.index(index='b', doc_type='a', id=3, body={'foo': 0, 'name': 'DUM'})

    es.index(index='b', doc_type='a', id=4, body={'foo': 0, 'name': 'DUM'})
    es.index(index='b', doc_type='a', id=5, body={'foo': 0, 'name': 'DUM'})
    es.index(index='b', doc_type='a', id=6, body={'foo': 0, 'name': 'DUM'})

    es.index(index='b', doc_type='a', id=7, body={'foo': 1, 'name': 'GOT'})
    es.index(index='b', doc_type='a', id=8, body={'foo': 1, 'name': 'GOT'})


def get_result():
    s = search.filter('term', foo=0)
    s.aggs.bucket('demo', 'terms', field='foo')
    result = s.execute()
    import pdb;pdb.set_trace()
    print(100 * '*')


if __name__ == '__main__':
    # create_index()
    # get_result()
    poc()
    # testdata()

    # import pdb;pdb.set_trace()
    # ff = ps.aggs.bucket('accounts', 'terms', field='foo')
    # res = es.count(index="test_index")
    # q = ps.query(query2)
    # nn = q.execute()
    # import pdb;pdb.set_trace()
    # q.aggs.bucket('test', 'terms', field='foo', size=0)
    # pp = q.execute()
    # import pdb;pdb.set_trace()
    # print('')
    # # ps.aggs.bucket('per_tag', 'terms', field='foo')
    # # print((q.sort({'foo': {'order': 'asc'}}).execute().hits.hits)[2:4])
