class FetchDataFromEs(object):
    """
    Fetch and aggregate the data between the process.
    """

    def get_data_from_es(self, duration):
        """
        Fetch the Data from Elastic Search for the given Duration

        Args:
             duration (datetime object)

        Returns:
            Result json blob from elastic search
        """
        pass

    def parse_the_data_by_process(self, pre_process_data, post_process_data):
        """
        Compare the data between the two process and does aggregations like
        1 . Total Count (claims entered to the process)
        2. Total Success Count (Claims exited the process)
        3. Total Failed Count
        4. Total In Progress Count
        5. Dropped count (Differences between 2 process count for example if 10 claims entered
        process1 and 8 were entered to process2 then 2 will be the dropped count)

        Args:
            pre_process_data (json)
            post_process_data (json)

        Returns:
            Aggregated JSON
        """
        pass


class MongoPersist(object):
    def save(self):
        pass

    def update(self):
        pass

    def read(self):
        pass
